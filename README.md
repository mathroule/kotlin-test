# Kotlin Test
This app display a list of users and their associated posts.

It's my first attempt to use Kotlin in a real app. Some helpers class are still in pure Java.

The Project is based on master detail activity project of Android Studio and use:
- Kotlin as main language
- MVVM with Android Architecture
- RxJava 2
- Retrofit for API call
- Gson for JSON mapping
- Room for database persistence
- Dagger for dependency injection
- Picasso for image loading
- LeakCanary to detect memory leak
- Stetho to see network call and view database
- JUnit for unit testing
- Robolectric to run unit test without Android device
- Mockito for mocking methods
- MockWebServer for API mocking
- Espresso for instrumentation test
- Sample data for layout preview with consistent data

### App preview
The app is optimized for phone and tablet.

#### Phone
![Phone users list](https://bitbucket.org/mathroule/kotlin-test/raw/master/content/img_phone_1.jpg)
![Phone user posts list](https://bitbucket.org/mathroule/kotlin-test/raw/master/content/img_phone_2.jpg)

#### Tablet
![Tablet users list and user posts list](https://bitbucket.org/mathroule/kotlin-test/raw/master/content/img_tablet.jpg)

### Install app
#### Debug build
To install a debug build (with Stetho and LeakCanary enabled).
```
gradlew installDebug
```

#### Release build
To install a release build (signed and shrinked with Proguard).
```
gradlew installRelease
```

### Unit testing
To run unit test based on JUnit and run with Robolectric.
```
gradlew testReleaseUnitTest
```

### Instrumentation testing
To run instrumentation test based on Espresso.
```
gradlew connectedReleaseInstrumentationAndroidTest
```

### Quality check
To run quality check (Detekt, PMD, Checkstyle, Lint, ktlint, Jacoco) and then import results in SonarQube.
A SonarQube instance should run otherwise the last task will fail (SonarQube is not mandatory to generate all reports).
```
gradlew qualityCheck
```

### TODO
- Use a CI platform like Bitrise or Travis CI to automate the build process.
- Use Crashlytics to report crash on a remote server.
- Use AsyncListDiffer to populate RecyclerView adapters.
- Publish app on Google Play.
