package android.security;

@SuppressWarnings("unused")
public class NetworkSecurityPolicy {

    private static final NetworkSecurityPolicy INSTANCE = new NetworkSecurityPolicy();

    public boolean isCleartextTrafficPermitted() {
        return true;
    }

    public static NetworkSecurityPolicy getInstance() {
        return INSTANCE;
    }
}
