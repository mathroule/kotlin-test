package com.mathroule.kotlintest.test

import android.os.Build
import com.mathroule.kotlintest.BuildConfig
import com.mathroule.kotlintest.TestKotlinTestApplication
import org.junit.runners.model.InitializationError
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

class CustomRobolectricRunner @Throws(InitializationError::class)
constructor(testClass: Class<*>) : RobolectricTestRunner(testClass) {

    override fun buildGlobalConfig(): Config {
        return Config.Builder.defaults()
                .setApplication(TestKotlinTestApplication::class.java)
                .setConstants(BuildConfig::class.java)
                .setSdk(Build.VERSION_CODES.LOLLIPOP)
                .build()
    }
}
