package com.mathroule.kotlintest.test

import org.junit.runner.RunWith

@RunWith(CustomRobolectricRunner::class)
abstract class BaseTest
