package com.mathroule.kotlintest.test.fixture

import com.mathroule.kotlintest.domain.User

/**
 * Serialized user fixtures from file users.json.
 */

val user1 = User(1, "Leanne Graham", "Bret", "Sincere@april.biz", "1-770-736-8031 x56442")

val user2 = User(2, "Ervin Howell", "Antonette", "Shanna@melissa.tv", "010-692-6593 x09125")

val user3 = User(3, "Clementine Bauch", "Samantha", "Nathan@yesenia.net", "1-463-123-4447")

val user4 = User(4, "Patricia Lebsack", "Karianne", "Julianne.OConner@kory.org", "493-170-9623 x156")

val users = listOf(user1, user2, user3, user4)
