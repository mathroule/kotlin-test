package com.mathroule.kotlintest.helpers

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import com.mathroule.kotlintest.test.BaseTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import org.robolectric.Shadows.shadowOf

class IntentUtilsTest : BaseTest() {

    private var activity: AppCompatActivity? = null

    @Before
    fun setUp() {
        activity = Robolectric.setupActivity(AppCompatActivity::class.java)
    }

    @Test
    fun get_send_sms_end_should_return_intent() {
        val intent = IntentUtils.getSendSmsIntent("0601020304")
        assertEquals("android.intent.action.SENDTO", intent.action)
        assertEquals("smsto:0601020304", intent.data.toString())
    }

    @Test
    fun start_intent_with_known_action_should_start_intent() {
        IntentUtils.startIntentSafely(activity!!, Intent(Intent.ACTION_DIAL, Uri.parse("tel:0601020304")))

        val startedIntent = shadowOf(AppCompatActivity()).nextStartedActivity
        assertEquals("android.intent.action.DIAL", startedIntent.action)
        assertEquals("tel:0601020304", startedIntent.data.toString())
    }
}
