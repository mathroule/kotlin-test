package com.mathroule.kotlintest.repository

import com.mathroule.kotlintest.repository.local.PostDao
import com.mathroule.kotlintest.repository.local.UserDao
import com.mathroule.kotlintest.repository.remote.ApiService
import com.mathroule.kotlintest.test.BaseRepositoryTest
import com.mathroule.kotlintest.test.fixture.posts
import com.mathroule.kotlintest.test.fixture.users
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import java.util.ArrayList
import java.util.Collections

class PostRepositoryTest : BaseRepositoryTest() {

    private lateinit var postRepository: PostRepository

    private lateinit var apiService: ApiService

    private lateinit var postDao: PostDao

    private lateinit var userDao: UserDao

    @Before
    override fun setUp() {
        super.setUp()

        apiService = mock(ApiService::class.java)

        postDao = appDatabase.postDao()

        userDao = appDatabase.userDao()

        postRepository = PostRepository(apiService, postDao)
    }

    @Test
    fun get_post_posts_with_inserted_posts_should_return_a_post() {
        postRepository.getUserPosts(1)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(Collections.emptyList())

        userDao.insertAll(ArrayList(users))

        postDao.insertAll(ArrayList(posts))

        postRepository.getUserPosts(1)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(posts)

        postRepository.getUserPosts(2)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(Collections.emptyList())
    }

    @Test
    fun get_post_without_inserted_posts_should_not_return_a_post() {
        postRepository.getUserPosts(2)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(Collections.emptyList())
    }

    @Test
    fun refresh_user_posts_should_insert_posts() {
        `when`(apiService.getUserPosts(1))
                .thenReturn(Single.just(posts))

        userDao.insertAll(ArrayList(users))

        postDao.getUserPosts(1)
                .test()
                .assertValue(Collections.emptyList())

        postRepository.refreshPosts(1)
                .test()
                .assertComplete()
                .assertNoErrors()

        verify(apiService).getUserPosts(1)

        postDao.getUserPosts(1)
                .test()
                .assertValue(posts)
    }

    @Test
    fun refresh_posts_with_api_issue_should_not_insert_posts() {
        `when`(apiService.getUserPosts(1))
                .thenReturn(Single.error(Exception("Error while fetching user posts.")))

        userDao.insertAll(ArrayList(users))

        postDao.getUserPosts(1)
                .test()
                .assertValue(Collections.emptyList())

        postRepository.refreshPosts(1)
                .test()
                .assertNotComplete()
                .assertError(Exception::class.java)
                .assertErrorMessage("Error while fetching user posts.")

        verify(apiService).getUserPosts(1)

        postDao.getUserPosts(1)
                .test()
                .assertValue(Collections.emptyList())
    }
}
