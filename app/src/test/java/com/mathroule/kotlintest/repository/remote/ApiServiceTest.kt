package com.mathroule.kotlintest.repository.remote

import com.google.common.base.Charsets
import com.google.common.io.Resources
import com.mathroule.kotlintest.test.BaseTest
import com.mathroule.kotlintest.test.fixture.posts
import com.mathroule.kotlintest.test.fixture.users
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException

class ApiServiceTest : BaseTest() {

    private lateinit var server: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun setUp() {
        server = MockWebServer()
        server.start()

        val url = server.url("")
        apiService = ApiService.create("${url.scheme()}://${url.host()}:${url.port()}")
    }

    @Test
    fun get_users_with_ok_should_return_a_list_of_user() {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(readFile("users.json")))

        apiService.getUsers()
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(users)

        val request = server.takeRequest()
        assertEquals("/users", request.path)
        assertEquals("GET", request.method)
    }

    @Test
    fun get_users_with_empty_users_should_return_an_empty_list() {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody("[]"))

        apiService.getUsers()
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(emptyList())

        val request = server.takeRequest()
        assertEquals("/users", request.path)
        assertEquals("GET", request.method)
    }

    @Test
    fun get_users_with_not_found_should_return_a_list_of_user() {
        server.enqueue(MockResponse()
                .setResponseCode(404))

        apiService.getUsers()
                .test()
                .assertError(HttpException::class.java)
                .assertErrorMessage("HTTP 404 Client Error")
                .assertNoValues()

        val request = server.takeRequest()
        assertEquals("/users", request.path)
        assertEquals("GET", request.method)
    }

    @Test
    fun get_user_posts_with_ok_should_return_a_list_of_post() {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody(readFile("posts.json")))

        apiService.getUserPosts(1)
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(posts)

        val request = server.takeRequest()
        assertEquals("/posts?userId=1", request.path)
        assertEquals("GET", request.method)
    }

    @Test
    fun get_user_posts_with_empty_posts_should_an_empty_list() {
        server.enqueue(MockResponse()
                .setResponseCode(200)
                .setBody("[]"))

        apiService.getUserPosts(45)
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(emptyList())

        val request = server.takeRequest()
        assertEquals("/posts?userId=45", request.path)
        assertEquals("GET", request.method)
    }

    @Test
    fun get_user_posts_with_not_found_should_return_an_error() {
        server.enqueue(MockResponse()
                .setResponseCode(404))

        apiService.getUserPosts(1)
                .test()
                .assertError(HttpException::class.java)
                .assertErrorMessage("HTTP 404 Client Error")
                .assertNoValues()

        val request = server.takeRequest()
        assertEquals("/posts?userId=1", request.path)
        assertEquals("GET", request.method)
    }

    private fun readFile(filename: String): String {
        return Resources.toString(Resources.getResource(filename), Charsets.UTF_8)
    }
}
