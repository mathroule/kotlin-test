package com.mathroule.kotlintest.repository

import com.mathroule.kotlintest.repository.local.UserDao
import com.mathroule.kotlintest.repository.remote.ApiService
import com.mathroule.kotlintest.test.BaseRepositoryTest
import com.mathroule.kotlintest.test.fixture.user2
import com.mathroule.kotlintest.test.fixture.users
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import java.util.ArrayList
import java.util.Collections

class UserRepositoryTest : BaseRepositoryTest() {

    private lateinit var userRepository: UserRepository

    private lateinit var apiService: ApiService

    private lateinit var userDao: UserDao

    @Before
    override fun setUp() {
        super.setUp()

        apiService = mock(ApiService::class.java)

        userDao = appDatabase.userDao()

        userRepository = UserRepository(apiService, userDao)
    }

    @Test
    fun get_user_with_inserted_users_should_return_an_user() {
        userRepository.getUser(2)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertNoValues()

        userDao.insertAll(ArrayList(users))

        userRepository.getUser(2)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(user2)
    }

    @Test
    fun get_user_without_inserted_users_should_not_return_an_user() {
        userRepository.getUser(2)
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertNoValues()
    }

    @Test
    fun get_users_with_inserted_users_should_return_users() {
        userRepository.getUsers()
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(Collections.emptyList())

        userDao.insertAll(ArrayList(users))

        userRepository.getUsers()
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(users)
    }

    @Test
    fun get_users_without_inserted_users_should_not_return_users() {
        userRepository.getUsers()
                .test()
                .assertNotComplete()
                .assertNoErrors()
                .assertValue(Collections.emptyList())
    }

    @Test
    fun refresh_users_should_insert_users() {
        `when`(apiService.getUsers())
                .thenReturn(Single.just(users))

        userDao.getUsers()
                .test()
                .assertValue(Collections.emptyList())

        userRepository.refreshUsers()
                .test()
                .assertComplete()
                .assertNoErrors()

        verify(apiService).getUsers()

        userDao.getUsers()
                .test()
                .assertValue(users)
    }

    @Test
    fun refresh_users_with_api_issue_should_not_insert_users() {
        `when`(apiService.getUsers())
                .thenReturn(Single.error(Exception("Error while fetching users.")))

        userDao.getUsers()
                .test()
                .assertValue(Collections.emptyList())

        userRepository.refreshUsers()
                .test()
                .assertNotComplete()
                .assertError(Exception::class.java)
                .assertErrorMessage("Error while fetching users.")

        verify(apiService).getUsers()

        userDao.getUsers()
                .test()
                .assertValue(Collections.emptyList())
    }
}
