package com.mathroule.kotlintest.injection

import android.content.Context
import com.jakewharton.picasso.OkHttp3Downloader
import com.mathroule.kotlintest.exception.LoadingPictureException
import com.mathroule.kotlintest.helpers.ErrorHandler
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    internal fun providePicasso(context: Context, okHttp3Downloader: OkHttp3Downloader): Picasso = Picasso.Builder(context)
            .downloader(okHttp3Downloader)
            .listener { _, uri, exception -> ErrorHandler.logException(LoadingPictureException(uri, exception)) }
            .build()

    @Singleton
    @Provides
    internal fun provideOkHttp3Downloader(okHttpClient: OkHttpClient): OkHttp3Downloader = OkHttp3Downloader(okHttpClient)

    @Singleton
    @Provides
    internal fun provideOkHttpClient(): OkHttpClient = OkHttpClient()
}
