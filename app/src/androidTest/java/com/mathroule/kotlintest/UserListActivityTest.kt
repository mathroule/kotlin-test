package com.mathroule.kotlintest

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import com.mathroule.kotlintest.helpers.action.CustomViewAction.waitForViewToAppear
import com.mathroule.kotlintest.helpers.matcher.RecyclerViewMatcher.withRecyclerView
import com.mathroule.kotlintest.helpers.matcher.ToolbarMatcher.withToolbarTitle
import com.mathroule.kotlintest.view.activity.UserListActivity
import org.junit.Rule
import org.junit.Test

class UserListActivityTest {

    @Rule
    @JvmField
    val activity = ActivityTestRule<UserListActivity>(UserListActivity::class.java)

    @Test
    fun test_with_user_posts() {
        waitForViewToAppear(R.id.recyclerViewUsers)

        onView(withRecyclerView(R.id.recyclerViewUsers).atPosition(3)).perform(click())

        waitForViewToAppear(R.id.recyclerViewUserPosts)

        onView(withId(R.id.detail_toolbar)).check(matches(withToolbarTitle("All user posts")))
    }
}
