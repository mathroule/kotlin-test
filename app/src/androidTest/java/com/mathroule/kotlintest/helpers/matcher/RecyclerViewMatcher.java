package com.mathroule.kotlintest.helpers.matcher;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class RecyclerViewMatcher {

    private final int recyclerViewId;

    private final String recyclerViewTag;

    private RecyclerViewMatcher(int recyclerViewId, String recyclerViewTag) {
        this.recyclerViewId = recyclerViewId;
        this.recyclerViewTag = recyclerViewTag;
    }

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId, null);
    }

    public static RecyclerViewMatcher withRecyclerView(final String recyclerViewTag) {
        return new RecyclerViewMatcher(0, recyclerViewTag);
    }

    public Matcher<View> atPosition(final int position) {
        return atPositionOnView(position, -1);
    }

    private Matcher<View> atPositionOnView(final int position, final int targetViewId) {
        return new TypeSafeMatcher<View>() {

            private Resources resources = null;

            private View childView;

            @Override
            public void describeTo(Description description) {
                String idDescription = Integer.toString(recyclerViewId);
                if (this.resources != null) {
                    try {
                        idDescription = this.resources.getResourceName(recyclerViewId);
                    } catch (Resources.NotFoundException e) {
                        idDescription = String.format("%s (resource name not found)", recyclerViewId);
                    }
                }

                description.appendText("RecyclerView with id: " + idDescription + " at position: " + position);
            }

            @Override
            public boolean matchesSafely(View view) {
                this.resources = view.getResources();

                if (childView == null) {
                    final RecyclerView recyclerView = TextUtils.isEmpty(recyclerViewTag)
                                                      ? view.getRootView().findViewById(recyclerViewId)
                                                      : view.getRootView().findViewWithTag(recyclerViewTag);
                    if (recyclerView != null && (recyclerView.getId() == recyclerViewId || (recyclerView.getTag() != null && recyclerView.getTag().equals(recyclerViewTag)))) {
                        RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                        if (viewHolder != null) {
                            childView = viewHolder.itemView;
                        }
                    } else {
                        return false;
                    }
                }

                if (targetViewId == -1) {
                    return view == childView;
                } else {
                    View targetView = childView != null ? childView.findViewById(targetViewId) : null;
                    return view == targetView;
                }
            }
        };
    }
}
