package com.mathroule.kotlintest.helpers.action;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.util.TreeIterables;
import android.view.View;

import org.hamcrest.Matcher;

import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class CustomViewAction {

    /**
     * Perform action of waiting for a specific view id.
     *
     * @param id the view id.
     */
    public static void waitForViewToAppear(final int id) {
        onView(isRoot()).perform(waitId(id));
    }

    /**
     * Perform action of waiting for a specific view id during 15 seconds.
     *
     * @param id the view id.
     * @return the ViewActionUtils
     */
    private static ViewAction waitId(final int id) {
        return waitId(id, TimeUnit.SECONDS.toMillis(15));
    }

    /**
     * Perform action of waiting for a specific view id.
     *
     * @param id      the view id.
     * @param timeout the timeout in millisecond.
     * @return the ViewActionUtils
     */
    private static ViewAction waitId(final int id, final long timeout) {
        return new ViewAction() {
            @Override
            public void perform(final UiController uiController, final View view) {
                uiController.loopMainThreadUntilIdle();
                final long startTime = System.currentTimeMillis();
                final long endTime = startTime + timeout;
                final Matcher<View> viewMatcher = withId(id);

                do {
                    for (View child : TreeIterables.breadthFirstViewTraversal(view)) {
                        if (viewMatcher.matches(child) && child.getVisibility() == View.VISIBLE) {
                            return;
                        }
                    }

                    uiController.loopMainThreadForAtLeast(50);
                }
                while (System.currentTimeMillis() < endTime);
            }

            @Override
            public String getDescription() {
                return "Wait for a view with id <" + id + "> during " + timeout + " millis.";
            }

            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }
        };
    }
}
