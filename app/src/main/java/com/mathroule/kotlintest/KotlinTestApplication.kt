package com.mathroule.kotlintest

import android.app.Application
import com.mathroule.kotlintest.injection.AppComponent
import com.squareup.picasso.Picasso
import javax.inject.Inject

open class KotlinTestApplication : Application() {

    @Inject
    lateinit var picasso: Picasso

    override fun onCreate() {
        super.onCreate()
        appComponent = AppComponent.create(this)
        appComponent.inject(this)

        initPicasso()
    }

    open fun initPicasso() {
        Picasso.setSingletonInstance(picasso)
    }

    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent
    }
}
