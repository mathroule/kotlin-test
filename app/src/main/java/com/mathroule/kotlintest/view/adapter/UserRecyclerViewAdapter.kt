package com.mathroule.kotlintest.view.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.helpers.getImageUrl
import com.mathroule.kotlintest.helpers.loadThumb
import com.mathroule.kotlintest.view.activity.UserListActivity
import com.mathroule.kotlintest.view.activity.UserPostListActivity
import com.mathroule.kotlintest.view.fragment.UserPostListFragment
import kotlinx.android.synthetic.main.user_list_content.view.imageViewProfile
import kotlinx.android.synthetic.main.user_list_content.view.textViewEmail
import kotlinx.android.synthetic.main.user_list_content.view.textViewName
import kotlinx.android.synthetic.main.user_list_content.view.textViewPhone
import kotlinx.android.synthetic.main.user_list_content.view.textViewUsername

class UserRecyclerViewAdapter(private val parentActivity: UserListActivity,
                              private val values: List<User>,
                              private val twoPane: Boolean) :
        RecyclerView.Adapter<UserRecyclerViewAdapter.ViewHolder>() {

    private val context: Context

    private val onClickListener: View.OnClickListener

    init {
        context = parentActivity

        onClickListener = View.OnClickListener { v ->
            val user = v.tag as User
            if (twoPane) {
                val fragment = UserPostListFragment().apply {
                    arguments = Bundle().apply {
                        putLong(UserPostListFragment.ARG_USER_ID, user.id)
                    }
                }
                parentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frameLayoutUserPosts, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, UserPostListActivity::class.java).apply {
                    putExtra(UserPostListFragment.ARG_USER_ID, user.id)
                }

                val pairImageView = Pair.create<View, String>(v.imageViewProfile, "user_profile_picture")
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(parentActivity, pairImageView)

                v.context.startActivity(intent, options.toBundle())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.user_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = values[position]

        holder.imageViewProfile.loadThumb(context, user.getImageUrl())
        holder.textViewName.text = user.name
        holder.textViewUserName.text = user.username
        holder.textViewEmail.text = user.email
        holder.textViewPhone.text = user.phone

        with(holder.itemView) {
            tag = user
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageViewProfile: ImageView = view.imageViewProfile
        val textViewName: TextView = view.textViewName
        val textViewUserName: TextView = view.textViewUsername
        val textViewEmail: TextView = view.textViewEmail
        val textViewPhone: TextView = view.textViewPhone
    }
}
