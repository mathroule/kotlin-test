package com.mathroule.kotlintest.view.activity

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.mathroule.kotlintest.KotlinTestApplication
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.helpers.ErrorHandler
import com.mathroule.kotlintest.helpers.NotifierHandler
import com.mathroule.kotlintest.helpers.SchedulerProvider
import com.mathroule.kotlintest.view.LoadView
import com.mathroule.kotlintest.view.adapter.UserRecyclerViewAdapter
import com.mathroule.kotlintest.view.model.UserListViewModel
import kotlinx.android.synthetic.main.activity_user_list.toolbar
import kotlinx.android.synthetic.main.user_list.frameLayoutUserPosts
import kotlinx.android.synthetic.main.user_list.recyclerViewUsers
import kotlinx.android.synthetic.main.user_list.swipeRefreshLayoutUsers
import kotlinx.android.synthetic.main.user_list.textViewNoUsers

/**
 * An activity representing a list of {@link User}.
 */
class UserListActivity : BaseActivity(), LoadView {

    private var userListViewModel: UserListViewModel? = null

    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        setSupportActionBar(toolbar)
        title = getString(R.string.title_user_list)
        toolbar.title = title

        if (frameLayoutUserPosts != null) {
            twoPane = true
        }

        val itemDecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)
        recyclerViewUsers.addItemDecoration(itemDecorator)

        userListViewModel = UserListViewModel.create(this)
        KotlinTestApplication.appComponent.inject(userListViewModel!!)

        userListViewModel?.let {
            subscribe(it.getUsers()
                    .subscribeOn(SchedulerProvider.io())
                    .observeOn(SchedulerProvider.ui())
                    .doOnError { ErrorHandler.logException(it) }
                    .subscribe { setUsers(it) })
        }

        onRefresh()

        swipeRefreshLayoutUsers.setOnRefreshListener { onRefresh() }
    }

    override fun showLoading() {
        swipeRefreshLayoutUsers.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayoutUsers.isRefreshing = false
    }

    private fun onRefresh() {
        userListViewModel?.let {
            subscribe(it.refreshUsers()
                    .subscribeOn(SchedulerProvider.io())
                    .observeOn(SchedulerProvider.ui())
                    .doOnSubscribe { showLoading() }
                    .doOnComplete { hideLoading() }
                    .doOnError {
                        hideLoading()

                        showError(getString(R.string.error_while_loading_users))

                        ErrorHandler.logException(it)
                    }
                    .doOnDispose { hideLoading() }
                    .subscribe())
        }
    }

    private fun setUsers(users: List<User>) {
        recyclerViewUsers.adapter = UserRecyclerViewAdapter(this, users, twoPane)

        if (users.isEmpty()) {
            textViewNoUsers.visibility = VISIBLE
            recyclerViewUsers.visibility = GONE
        } else {
            textViewNoUsers.visibility = GONE
            recyclerViewUsers.visibility = VISIBLE
        }
    }

    private fun showError(message: String) {
        NotifierHandler.show(recyclerViewUsers, message, R.string.retry, View.OnClickListener { onRefresh() })
    }
}
