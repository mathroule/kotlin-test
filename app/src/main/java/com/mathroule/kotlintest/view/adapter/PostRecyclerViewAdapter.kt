package com.mathroule.kotlintest.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.Post
import kotlinx.android.synthetic.main.post_list_content.view.*

class PostRecyclerViewAdapter(private val values: List<Post>) :
        RecyclerView.Adapter<PostRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.post_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = values[position]
        holder.textViewTitle.text = post.title
        holder.textViewBody.text = post.body

        with(holder.itemView) {
            tag = post
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewTitle: TextView = view.textViewTitle
        val textViewBody: TextView = view.textViewBody
    }
}
