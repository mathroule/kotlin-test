package com.mathroule.kotlintest.view

interface LoadView {

    fun showLoading()

    fun hideLoading()
}
