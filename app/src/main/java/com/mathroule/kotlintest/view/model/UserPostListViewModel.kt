package com.mathroule.kotlintest.view.model

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import com.mathroule.kotlintest.domain.Post
import com.mathroule.kotlintest.repository.PostRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class UserPostListViewModel : ViewModel() {

    @Inject
    lateinit var postRepository: PostRepository

    fun getUserPosts(userId: Long): Flowable<List<Post>> = postRepository.getUserPosts(userId)

    fun refreshPosts(userId: Long): Completable = postRepository.refreshPosts(userId)

    companion object {
        fun create(fragment: Fragment): UserPostListViewModel = ViewModelProviders.of(fragment).get(UserPostListViewModel::class.java)
    }
}
