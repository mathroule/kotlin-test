package com.mathroule.kotlintest.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.view.MenuItem
import com.mathroule.kotlintest.KotlinTestApplication
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.helpers.ErrorHandler
import com.mathroule.kotlintest.helpers.IntentUtils
import com.mathroule.kotlintest.helpers.SchedulerProvider
import com.mathroule.kotlintest.helpers.getImageUrl
import com.mathroule.kotlintest.helpers.load
import com.mathroule.kotlintest.view.fragment.UserPostListFragment
import com.mathroule.kotlintest.view.model.UserViewModel
import kotlinx.android.synthetic.main.activity_user_post_list.detail_toolbar
import kotlinx.android.synthetic.main.activity_user_post_list.fab
import kotlinx.android.synthetic.main.activity_user_post_list.imageViewProfile
import kotlinx.android.synthetic.main.activity_user_post_list.toolbar_layout

/**
 * An activity representing a list of {@link Post} for an {@link User}.
 */
class UserPostListActivity : BaseActivity() {

    private var userViewModel: UserViewModel? = null

    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_post_list)
        setSupportActionBar(detail_toolbar)

        fab.setOnClickListener {
            user?.let {
                IntentUtils.startIntentSafely(this, IntentUtils.getSendSmsIntent(it.phone))
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = UserPostListFragment().apply {
                arguments = Bundle().apply {
                    putLong(UserPostListFragment.ARG_USER_ID,
                            intent.getLongExtra(UserPostListFragment.ARG_USER_ID, 0))
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.frameLayoutUserPosts, fragment)
                    .commit()
        }

        userViewModel = UserViewModel.create(this)
        KotlinTestApplication.appComponent.inject(userViewModel!!)

        intent?.let { intent ->
            if (intent.hasExtra(UserPostListFragment.ARG_USER_ID)) {
                userViewModel?.let { userViewModel ->
                    subscribe(userViewModel.getUser(intent.getLongExtra(UserPostListFragment.ARG_USER_ID, 0))
                            .subscribeOn(SchedulerProvider.io())
                            .observeOn(SchedulerProvider.ui())
                            .doOnError { ErrorHandler.logException(it) }
                            .subscribe {
                                toolbar_layout.title = it.name

                                imageViewProfile.load(this, it.getImageUrl())

                                user = it
                            })
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    NavUtils.navigateUpTo(this, Intent(this, UserListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
}
