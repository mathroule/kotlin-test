package com.mathroule.kotlintest.view.model

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.repository.UserRepository
import io.reactivex.Flowable
import javax.inject.Inject

class UserViewModel : ViewModel() {

    @Inject
    lateinit var userRepository: UserRepository

    fun getUser(userId: Long): Flowable<User> = userRepository.getUser(userId)

    companion object {
        fun create(activity: FragmentActivity): UserViewModel = ViewModelProviders.of(activity).get(UserViewModel::class.java)
    }
}
