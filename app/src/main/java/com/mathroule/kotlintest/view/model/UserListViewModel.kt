package com.mathroule.kotlintest.view.model

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class UserListViewModel : ViewModel() {

    @Inject
    lateinit var userRepository: UserRepository

    fun getUsers(): Flowable<List<User>> = userRepository.getUsers()

    fun refreshUsers(): Completable = userRepository.refreshUsers()

    companion object {
        fun create(activity: FragmentActivity): UserListViewModel = ViewModelProviders.of(activity).get(UserListViewModel::class.java)
    }
}
