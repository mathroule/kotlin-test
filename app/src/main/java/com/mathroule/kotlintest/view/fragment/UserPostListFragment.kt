package com.mathroule.kotlintest.view.fragment

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mathroule.kotlintest.KotlinTestApplication
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.Post
import com.mathroule.kotlintest.helpers.ErrorHandler
import com.mathroule.kotlintest.helpers.NotifierHandler
import com.mathroule.kotlintest.helpers.SchedulerProvider
import com.mathroule.kotlintest.view.LoadView
import com.mathroule.kotlintest.view.adapter.PostRecyclerViewAdapter
import com.mathroule.kotlintest.view.model.UserPostListViewModel
import kotlinx.android.synthetic.main.fragment_user_post_list.recyclerViewUserPosts
import kotlinx.android.synthetic.main.fragment_user_post_list.swipeRefreshLayoutUserPosts
import kotlinx.android.synthetic.main.fragment_user_post_list.textViewNoUserPosts

/**
 * A fragment representing a list of {@link Post} for an {@link User}.
 */
class UserPostListFragment : BaseFragment(), LoadView {

    private var userPostListViewModel: UserPostListViewModel? = null

    private var userId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userPostListViewModel = UserPostListViewModel.create(this)
        KotlinTestApplication.appComponent.inject(userPostListViewModel!!)

        arguments?.let {
            if (it.containsKey(ARG_USER_ID)) {
                userId = it.getLong(ARG_USER_ID)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_user_post_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val itemDecorator = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.divider)!!)
        recyclerViewUserPosts.addItemDecoration(itemDecorator)

        swipeRefreshLayoutUserPosts.setOnRefreshListener { onRefresh() }

        userId?.let { userId ->
            userPostListViewModel?.let { userPostListViewModel ->
                subscribe(userPostListViewModel.getUserPosts(userId)
                        .subscribeOn(SchedulerProvider.io())
                        .observeOn(SchedulerProvider.ui())
                        .doOnError { ErrorHandler.logException(it) }
                        .subscribe { setUserPosts(it) })
            }
        }

        onRefresh()
    }

    override fun showLoading() {
        swipeRefreshLayoutUserPosts.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayoutUserPosts.isRefreshing = false
    }

    private fun setUserPosts(posts: List<Post>) {
        recyclerViewUserPosts.adapter = PostRecyclerViewAdapter(posts)

        if (posts.isEmpty()) {
            textViewNoUserPosts.visibility = View.VISIBLE
            recyclerViewUserPosts.visibility = View.GONE
        } else {
            textViewNoUserPosts.visibility = View.GONE
            recyclerViewUserPosts.visibility = View.VISIBLE
        }
    }

    private fun onRefresh() {
        userId?.let { userId ->
            userPostListViewModel?.let { userPostListViewModel ->
                subscribe(userPostListViewModel.refreshPosts(userId)
                        .subscribeOn(SchedulerProvider.io())
                        .observeOn(SchedulerProvider.ui())
                        .doOnSubscribe { showLoading() }
                        .doOnComplete { hideLoading() }
                        .doOnError {
                            hideLoading()

                            showError(getString(R.string.error_while_loading_user_posts))

                            ErrorHandler.logException(it)
                        }
                        .doOnDispose { hideLoading() }
                        .subscribe())
            }
        }
    }

    private fun showError(message: String) {
        NotifierHandler.show(recyclerViewUserPosts, message, R.string.retry, View.OnClickListener { onRefresh() })
    }

    companion object {
        const val ARG_USER_ID = "user_id"
    }
}
