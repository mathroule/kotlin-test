package com.mathroule.kotlintest.injection

import android.app.Application
import android.content.Context
import com.mathroule.kotlintest.KotlinTestApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule(val application: KotlinTestApplication) {

    @Provides
    internal fun provideApplication(): Application = application

    @Provides
    internal fun provideContext(): Context = application.applicationContext
}
