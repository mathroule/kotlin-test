package com.mathroule.kotlintest.injection

import com.mathroule.kotlintest.KotlinTestApplication
import com.mathroule.kotlintest.view.model.UserListViewModel
import com.mathroule.kotlintest.view.model.UserPostListViewModel
import com.mathroule.kotlintest.view.model.UserViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class), (RepositoryModule::class)])
interface AppComponent {

    fun inject(application: KotlinTestApplication)

    fun inject(viewModel: UserListViewModel)

    fun inject(viewModel: UserPostListViewModel)

    fun inject(viewModel: UserViewModel)

    companion object Factory {
        fun create(application: KotlinTestApplication): AppComponent = DaggerAppComponent.builder()
                .appModule(AppModule(application))
                .networkModule(NetworkModule())
                .repositoryModule(RepositoryModule())
                .build()
    }
}
