package com.mathroule.kotlintest.injection

import android.content.Context
import com.mathroule.kotlintest.repository.PostRepository
import com.mathroule.kotlintest.repository.UserRepository
import com.mathroule.kotlintest.repository.local.AppDatabase
import com.mathroule.kotlintest.repository.local.PostDao
import com.mathroule.kotlintest.repository.local.UserDao
import com.mathroule.kotlintest.repository.remote.ApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class RepositoryModule {

    companion object {
        const val API_URL = "https://jsonplaceholder.typicode.com/"
    }

    @Singleton
    @Provides
    internal fun provideApiService(okHttpClient: OkHttpClient): ApiService = ApiService.create(API_URL, okHttpClient)

    @Singleton
    @Provides
    internal fun provideAppDatabase(context: Context): AppDatabase = AppDatabase.getInstance(context)!!

    @Singleton
    @Provides
    internal fun providePostDao(appDatabase: AppDatabase): PostDao = appDatabase.postDao()

    @Singleton
    @Provides
    internal fun provideUserDao(appDatabase: AppDatabase): UserDao = appDatabase.userDao()

    @Singleton
    @Provides
    internal fun providePostRepository(apiService: ApiService, postDao: PostDao): PostRepository = PostRepository(apiService, postDao)

    @Singleton
    @Provides
    internal fun provideUserRepository(apiService: ApiService, userDao: UserDao): UserRepository = UserRepository(apiService, userDao)
}
