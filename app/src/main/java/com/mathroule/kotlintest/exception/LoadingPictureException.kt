package com.mathroule.kotlintest.exception

import android.net.Uri

/**
 * Exception thrown while loading picture.
 */
class LoadingPictureException(uri: Uri, cause: Throwable) : Exception("Error while loading picture (Uri: '$uri').", cause)
