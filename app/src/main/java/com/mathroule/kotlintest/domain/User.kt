package com.mathroule.kotlintest.domain

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
data class User(@PrimaryKey val id: Long, val name: String, val username: String, val email: String, val phone: String)
