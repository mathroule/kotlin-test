package com.mathroule.kotlintest.domain

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "posts",
        indices = [(Index(value = ["user_id"]))],
        foreignKeys = [(ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["user_id"], onDelete = CASCADE))])
data class Post(@PrimaryKey val id: Long, @ColumnInfo(name = "user_id") val userId: Long, val title: String, val body: String)
