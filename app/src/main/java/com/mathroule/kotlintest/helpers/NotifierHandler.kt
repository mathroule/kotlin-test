package com.mathroule.kotlintest.helpers

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View

object NotifierHandler {

    fun show(view: View, message: CharSequence, @StringRes action: Int, listener: View.OnClickListener?) {
        Snackbar.make(view, message, 0).setAction(action, listener).show()
    }
}
