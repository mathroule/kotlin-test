package com.mathroule.kotlintest.helpers

import android.content.Context
import android.widget.ImageView
import com.mathroule.kotlintest.R
import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.helpers.transformation.CircleTransformation
import com.squareup.picasso.Picasso

fun ImageView.loadThumb(context: Context, url: String) {
    Picasso
            .with(context)
            .load(url)
            .fit()
            .centerInside()
            .placeholder(R.drawable.ic_user_placeholder)
            .transform(CircleTransformation())
            .into(this)
}

fun ImageView.load(context: Context, url: String) {
    Picasso
            .with(context)
            .load(url)
            .fit()
            .centerCrop()
            .placeholder(R.drawable.ic_user_placeholder)
            .into(this)
}

fun User.getImageUrl(): String = "https://picsum.photos/300/300/?image=20${this.id}"
