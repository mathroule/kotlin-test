package com.mathroule.kotlintest.helpers

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

object IntentUtils {

    fun getSendSmsIntent(number: String): Intent = Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:$number"))

    fun startIntentSafely(context: Context, intent: Intent) {
        try {
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            ErrorHandler.logException(e)
        }
    }
}
