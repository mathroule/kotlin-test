package com.mathroule.kotlintest.helpers

import android.util.Log

object ErrorHandler {

    // TODO use Crashlytics to report crash

    private const val TAG = "ErrorHandler"

    fun logException(throwable: Throwable) {
        Log.e(TAG, "Error", throwable)
    }
}
