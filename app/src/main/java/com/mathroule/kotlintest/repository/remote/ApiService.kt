package com.mathroule.kotlintest.repository.remote

import com.mathroule.kotlintest.domain.Post
import com.mathroule.kotlintest.domain.User
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("users")
    fun getUsers(): Single<List<User>>

    @GET("posts")
    fun getUserPosts(@Query("userId") userId: Long): Single<List<Post>>

    companion object {
        fun create(baseUrl: String, okHttpClient: OkHttpClient = OkHttpClient()): ApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}
