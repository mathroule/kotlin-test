package com.mathroule.kotlintest.repository.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.mathroule.kotlintest.domain.User
import io.reactivex.Flowable

@Dao
abstract class UserDao : BaseDao<User>() {

    /**
     * Get an user from the users table.
     */
    @Query("SELECT * FROM users WHERE id = :userId LIMIT 1")
    abstract fun getUser(userId: Long): Flowable<User>

    /**
     * Get all users from the users table.
     */
    @Query("SELECT * FROM users")
    abstract fun getUsers(): Flowable<List<User>>
}
