package com.mathroule.kotlintest.repository.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

@Dao
abstract class BaseDao<T> {

    /**
     * Insert an item in the database.
     *
     * @param item the item to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: T)

    /**
     * Insert all items into database.
     *
     * @param items the items to create.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(items: ArrayList<T>)

    /**
     * Update an item from the database.
     *
     * @param item the item to be updated.
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(item: T)

    /**
     * Update a list of items from the database.
     *
     * @param items the items to be updated.
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateAll(items: ArrayList<T>)

    /**
     * Delete an item from the database.
     *
     * @param item the item to be deleted.
     */
    @Delete
    abstract fun delete(item: T)
}
