package com.mathroule.kotlintest.repository

import com.mathroule.kotlintest.domain.Post
import com.mathroule.kotlintest.repository.local.PostDao
import com.mathroule.kotlintest.repository.remote.ApiService
import io.reactivex.Completable
import io.reactivex.Flowable

class PostRepository(val apiService: ApiService, val postDao: PostDao) {

    fun getUserPosts(userId: Long): Flowable<List<Post>> = postDao.getUserPosts(userId)
            .distinctUntilChanged()

    fun refreshPosts(userId: Long): Completable = apiService.getUserPosts(userId)
            .flatMapCompletable { insertPosts(it) }

    private fun insertPosts(posts: List<Post>): Completable = Completable.fromAction {
        postDao.insertAll(ArrayList(posts))
    }
}
