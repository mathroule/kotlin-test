package com.mathroule.kotlintest.repository

import com.mathroule.kotlintest.domain.User
import com.mathroule.kotlintest.repository.local.UserDao
import com.mathroule.kotlintest.repository.remote.ApiService
import io.reactivex.Completable
import io.reactivex.Flowable

class UserRepository(val apiService: ApiService, val userDao: UserDao) {

    fun getUser(userId: Long): Flowable<User> = userDao.getUser(userId)
            .distinctUntilChanged()

    fun getUsers(): Flowable<List<User>> = userDao.getUsers()
            .distinctUntilChanged()

    fun refreshUsers(): Completable = apiService.getUsers()
            .flatMapCompletable { insertUsers(it) }

    private fun insertUsers(users: List<User>): Completable = Completable.fromAction {
        userDao.insertAll(ArrayList(users))
    }
}
