package com.mathroule.kotlintest.repository.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.mathroule.kotlintest.domain.Post
import io.reactivex.Flowable

@Dao
abstract class PostDao : BaseDao<Post>() {

    /**
     * Get all posts for an user from the posts table.
     */
    @Query("SELECT * FROM posts WHERE user_id = :userId")
    abstract fun getUserPosts(userId: Long): Flowable<List<Post>>
}
