# Test
-dontobfuscate

-keep class com.mathroule.kotlintest.** { *; }

-keep public class * extends android.app.**
-keep public class * extends android.test.**

-dontnote junit.framework.**
-dontnote junit.runner.**
-dontnote org.junit.**

-dontwarn android.test.**
-keep class android.test.** { *; }
-keep class android.app.** { *; }
-keep class android.support.test.** { *; }
-dontwarn com.squareup.**
-keep class com.squareup.** { *;}
-dontwarn org.bouncycastle.**
-keep class org.bouncycastle.** { *; }
-dontwarn org.hamcrest.**
-dontwarn org.junit.**
-keep class org.junit.** { *; }
-dontwarn org.mockito.**
-keep class org.mockito.** { *; }
